# Google Cloud Platform の Kubernetes(GKE) にデプロイ

## k8sのインスタンスを生成

- リージョンを`us-central` に設定（たぶん一番安いっぽい）

```
$ gcloud config set compute/zone us-central1-f
```

- GKE のインスタンスを生成
  - 最小限の構成
    - `g1-small` では Istio や Spinnaker を起動できなかった、、、、
  - 時間単位で課金が起きるもの
    - このハンズオンで、100円程度はかかる可能性があります
      - 「予算とアラート」を設定しておくと安心
  - `ERROR: (gcloud.container.clusters.create) ResponseError: code=403, message=Kubernetes Engine API is not enabled for this project. Please ensure it is enabled in Google Cloud Console and try again: visit xxxxx` というエラーで失敗する場合がある
  - その場合は、GUIのコンソールで GKE のページにアクセスすると Kubernetes Engine API が有効になるので、再度試す

```
$ gcloud container clusters create devops-test \
  --num-nodes 3 \
  --machine-type n1-standard-2
$ kubectl cluster-info
```

## サンプルのコードをデプロイ


- Cloud Shellを開き、先程 Fork したコードを取得する

```
$ cd
$ git clone https://gitlab.com/{your_name}/devops-test.git
# $ git clone https://gitlab.com/koda3t/devops-test.git
```

- Dockerイメージをビルドして、 Container Registry にプッシュ

```
$ cd devops-test
$ export PROJECT=$(gcloud info --format='value(config.project)')
$ docker build -t gcr.io/$PROJECT/devops-test:v0.0.1 web/.
$ docker push gcr.io/$PROJECT/devops-test:v0.0.1
```

- Container Registry にイメージが登録されていることを確認（GUI）


- サンプルの設定の「コンテナイメージ名」を修正した設定ファイルを作成する

```
$ export PROJECT=$(gcloud info --format='value(config.project)')
$ sed -i s/devops-test/devops-test:v0.0.1/g k8s/deployment*
$ sed -i s/project_name/$PROJECT/g k8s/deployment*
$ cat k8s/deployment.yaml
```

```
$ kubectl apply -f k8s/deployment.yaml
$ kubectl apply -f k8s/deployment-canary.yaml
$ kubectl apply -f k8s/service.yaml
$ kubectl get pod
$ kubectl get service
```

- ブラウザでアクセスできることを確認
  - `kubectl get service` で確認した、EXTERNAL-IP
  - 何回かアクセスすると、 `production` と `canary` に切り替わる


## クリーンアップ

```
$ kubectl delete -f k8s/deployment.yaml
$ kubectl delete -f k8s/deployment-canary.yaml
$ kubectl delete -f k8s/service.yaml
$ kubectl get pod
$ kubectl get service
$ git checkout .
```

**おつかれさまでした。次のセクションに進みます。**
